using System.Collections;
using System.Collections.Generic;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;
namespace PhotonManager {
    public class NetwokManager : MonoBehaviourPunCallbacks
    {
        public Text StatusText;
        public InputField NickNameInput;
        public Button startButton;
        public GameObject startPanel;

        public string gameVersion = "1.0";

        private List<Transform> positionsList = new List<Transform>();
        private int idx;

        public InputField ChatInput;
        public GameObject chatPanel, chatView;
        public PhotonView PV;

        private Text[] chatList;
        private void OnDestroy()
        {
            PhotonNetwork.LeaveRoom();
        }
        void Awake()
        {
            PhotonNetwork.AutomaticallySyncScene = true;
            //PhotonNetwork.autoJoinLobby = true;
        }

        void Start()
        {
            Debug.Log("Start");
            Transform[] positions = GameObject.Find("SpawnPosition").GetComponentsInChildren<Transform>();
            foreach (Transform pos in positions)
                positionsList.Add(pos);

            startButton.onClick.AddListener(JoinRoom);
            OnLogin();

            chatList = chatView.GetComponentsInChildren<Text>();

        }
        void OnLogin()
        {
            Debug.Log("OnLogin");
            Connect();
            startButton.interactable = false;
            StatusText.text = "마스터 서버에 접속중...";
        }

        void JoinRoom()
        {
            Debug.Log("JoinRoom");
            if (NickNameInput.text.Equals(""))
                PhotonNetwork.LocalPlayer.NickName = "unknown";
            else
                PhotonNetwork.LocalPlayer.NickName = NickNameInput.text;
            PhotonNetwork.JoinRandomRoom();
        }

        public void Connect() => PhotonNetwork.ConnectUsingSettings();

        public override void OnConnectedToMaster()
        {
            Debug.Log("OnConnectedToMaster");
            StatusText.text = "Online: 마스터 서버와 연결 됨";
            startButton.interactable = true;
        }

        public override void OnJoinedRoom()
        {
            Debug.Log("OnJoinedRoom");
            if (PhotonNetwork.IsMasterClient)
            {
                PV.RPC("CreateComputerPlayer", RpcTarget.All);
            }
            idx = Random.Range(0, positionsList.Count);
            PhotonNetwork.Instantiate("Player3", positionsList[idx].position, Quaternion.identity);
            positionsList.RemoveAt(idx);
            startPanel.SetActive(false);

            chatPanel.SetActive(true);

            ChatInput.text = "";
            foreach (Text chat in chatList)
                chat.text = "";

            if (PhotonNetwork.IsMasterClient)
            {
                PV.RPC("ChatRPC", RpcTarget.All, "<color=yellow>[방장] " + PhotonNetwork.NickName + "님이 참가하셨습니다</color>");
            }
        }

        [PunRPC]
        void CreateComputerPlayer()
        {
            Debug.Log("CreateComputerPlayer");
            for (int i = 0; i < 10; i++)
            {
                idx = Random.Range(0, positionsList.Count);
                PhotonNetwork.Instantiate("Computer Player", positionsList[idx].position, Quaternion.identity);
                positionsList.RemoveAt(idx);
            }
        }

        public override void OnJoinRandomFailed(short returnCode, string message)
        {
            Debug.Log("OnJoinRandomFailed");
            this.CreateRoom();
        }

        void CreateRoom()
        {
            Debug.Log("CreateRoom");
            PhotonNetwork.CreateRoom(null, new RoomOptions { MaxPlayers = 4 });
        }

        // 플레이어 접속 시 채팅 창 출력
        public override void OnPlayerEnteredRoom(Player newPlayer)
        {
            Debug.Log("OnPlayerEnteredRoom");
            PV.RPC("ChatRPC", RpcTarget.All, "<color=yellow>" + newPlayer.NickName + "님이 참가하셨습니다</color>");
        }

        // 플레이어 퇴장 시 채팅 창 출력
        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            PV.RPC("ChatRPC", RpcTarget.All, "<color=yellow>" + otherPlayer.NickName + "님이 나갔습니다</color>");
        }

        // 채팅 전송 함수
        public void Send()
        {
            if (ChatInput.text.Equals(""))
                return;
            string msg = "[" + PhotonNetwork.NickName + "] " + ChatInput.text;
            PV.RPC("ChatRPC", RpcTarget.All, msg);
            ChatInput.text = "";
        }

        [PunRPC]
        void ChatRPC(string msg)
        {
            bool isInput = false;
            for (int i = 0; i < chatList.Length; i++)
                if (chatList[i].text == "")
                {
                    isInput = true;
                    chatList[i].text = msg;
                    break;
                }
            if (!isInput)
            {
                for (int i = 1; i < chatList.Length; i++) chatList[i - 1].text = chatList[i].text;
                chatList[chatList.Length - 1].text = msg;
            }
        }
    }
}
