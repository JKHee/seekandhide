using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComputerPlayer : MonoBehaviour
{

    float time;
    float rotateTime;
    float stopTime;
    float v;
    float h;
    int state = 0;

    public float speed = 5;
    public float turnSpeed = 70;
    Animator computer_animator;

    private float m_currentV = 0; //현재 가상 수직선 위치
    private float m_currentH = 0; //현재 가상 수평선 위치
    private readonly float m_interpolation = 10;
    // Start is called before the first frame update
    void Start()
    {
        computer_animator = transform.GetComponent<Animator>();
    }
    void SetRandom()
    {
        time = UnityEngine.Random.Range(4.0f, 7.0f);
        rotateTime = UnityEngine.Random.Range(2.0f, 3.0f);
        stopTime = UnityEngine.Random.Range(1.0f, 3.0f);
        v = UnityEngine.Random.Range(-1.0f, 1.0f);
        h = UnityEngine.Random.Range(-1.0f, 1.0f);
    }

    void Update()
    {
        /*
        state = 0 -> 이동
        state = 1 -> 회전
        state = 2 -> 정지
        */
        switch (state)
        {
            case 0: // 이동 -> 4초간 이동 후, 2초 정지
                if (time > 0)
                {
                    if (v < 0)
                    {
                        v *= 0.9f;
                    }

                    m_currentV = Mathf.Lerp(m_currentV, v, Time.deltaTime * m_interpolation);

                    this.transform.Translate(Vector3.forward * v * speed * Time.deltaTime);
                    this.transform.Rotate(Vector3.up * h * turnSpeed * Time.deltaTime);
                    computer_animator.SetFloat("Speed", m_currentV); //애니메이션 갱신
                    time -= Time.deltaTime;
                }
                else
                {
                    SetRandom();
                    state = 2;
                }
                break;
            case 1: // 회전 -> 2초간 회전 후, 이동 state
                if (rotateTime > 0)
                {
                    this.transform.Rotate(Vector3.up * h * turnSpeed * Time.deltaTime);
                    rotateTime -= Time.deltaTime;
                }
                else
                {
                    SetRandom();
                    state = 0;
                }
                break;
            case 2: // 2초간 정지 후, 0~1 state
                if (stopTime > 0)
                    stopTime -= Time.deltaTime;
                else
                {
                    SetRandom();
                    state = UnityEngine.Random.Range(0, 2);
                }
                break;
        }
    }
}
