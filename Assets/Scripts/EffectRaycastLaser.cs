using UnityEngine;
using Photon.Pun;

public class EffectRaycastLaser: MonoBehaviourPunCallbacks
{
    public GameObject point; //쏘는 위치
    public GameObject beam; //빔
    GameObject director;

    private void Start()
    {
        director = GameObject.Find("GameDirector");
        Debug.Log("123123 "+director);
    }

    void Update()
    {
        if (!photonView.IsMine) return;

        if (Input.GetMouseButtonDown(0)) //좌 클릭 시
        {
            photonView.RPC("Laser", RpcTarget.Others, null);
            Laser();
        }
    }

    [PunRPC]
    void Laser()
    {
        RaycastHit hit;
        //Debug.DrawRay(point.transform.position, beam.transform.forward * 1.0f, Color.red, 3.0f);
        if (Physics.Raycast(point.transform.position, beam.transform.forward, out hit, 4.0f))
        {
            if (hit.transform.tag == "Player")
            {
                Debug.Log("플레이어와 충돌!");
                GameObject player = hit.transform.gameObject;
                director.GetComponent<GameDirector>().PlayerDead(player);

            }
            else if (hit.transform.tag == "Computer")
            {
                Debug.Log("컴퓨터와 충돌!");
                GameObject computer = hit.transform.gameObject;
                director.GetComponent<GameDirector>().ComputerDead(computer);
            }
        }
        Debug.Log(hit.transform);
        beam.SetActive(true);
        beam.transform.localScale = new Vector3(1, 1, 40);
        Invoke("HideBeam", 1.0f);
    }
    void HideBeam()
    {
        beam.SetActive(false);
        beam.transform.localScale = new Vector3(1, 1, 1);
    }
}